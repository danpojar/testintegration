import QtQuick 2.6
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0
import atlasengine 1.0
import "."

Item {
    /**
     * @property translatedStrings - a map the holds the translated strings
     */
    //property variant translatedStrings: QtArp.localizationManager.translatedStrings

    anchors.fill: parent
    Item {
        anchors.fill: parent
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

        AtlasEngine {
            id: atlasView
            mirrorVertically: true
            anchors.fill: parent

            MouseArea {
                id: mouseArea
                anchors.fill: parent
                onPressed: atlasView.propagateMouseMove(mouseX, mouseY, true, false);
                onPositionChanged: atlasView.propagateMouseMove(mouseX, mouseY, false, false);
                onReleased: atlasView.propagateMouseMove(mouseX, mouseY, false, true);
                onWheel: {atlasView.propagateMouseWheel(wheel.angleDelta.y)}
            }
        }
    }
}
