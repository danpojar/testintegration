#include "atlasengine.h"

#include "atlasmapenginecommon.h"

#include <QDebug>
#include <QDir>
#include <QFunctionPointer>
#include <QGeoCoordinate>
#include <QOpenGLContext>
#include <QTime>
#include <chrono>
#include <cmath>
#include <string>

static void *getProcAddr(const char *funcName) {
  QFunctionPointer funcPtr =
      QOpenGLContext::currentContext()->getProcAddress(funcName);
  return (void *)(funcPtr);
}

namespace boost {
void throw_exception(std::exception const &e) {
  static TnLogTopic aTopic("boost_exception");
  TN_LOGT(TnInfo, aTopic) << e.what();
}
}

std::string AtlasInitParams::path = "";

QOpenGLFramebufferObject *
AtlasEngineRenderer::createFramebufferObject(const QSize &size) {
  QOpenGLFramebufferObjectFormat format;
  format.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
  format.setSamples(0);

  if (viewId) {
    m_engine->ResizeView(viewId, 0, 0, size.width(), size.height());
  }
  return new QOpenGLFramebufferObject(size, format);
}

void AtlasEngineRenderer::initViewSettings(
    tn::atlas::ViewSettings &viewSettings)

{
  tn::atlas::ConstOptionsPtr defaultViewOptions =
      m_engine->Settings().load("default.info");
  tn::atlas::ConstOptionsPtr mainViewSettings =
      m_engine->Settings().extend("mainViewOptions", defaultViewOptions);

  viewSettings.options = mainViewSettings;
  viewSettings.viewport.x = static_cast<int32_t>(0);
  viewSettings.viewport.y = static_cast<int32_t>(0);
  viewSettings.viewport.width = static_cast<uint32_t>(800 * 2);
  viewSettings.viewport.height = static_cast<uint32_t>(600 * 2);
  viewSettings.style = "newstyle.tss";
  viewSettings.viewport.dotsPerInch = 200.0f;
  // viewSettings.viewport.screenScale has been temporarily disabled
  // viewSettings.viewport.screenScale = m_atlasInitParams.screenScale;
  viewSettings.baseTileSize = 256;
  viewSettings.pos.setLatitude(46.77043);
  viewSettings.pos.setLongitude(23.59142);
}

void AtlasEngineRenderer::initData() {
  if (m_data)
    return;

  //        static TnHttpProxy
  //        trafficUrl(Tn::Url::UrlCreator::GetInstance()->CreateUrl());
  std::string const cloudURL =
      "http://autodenali-ngxtile.telenav.com/map/v0/ngx/NA/15Q1/";
  auto dataAccessptr = tn::map::DataUtility::AccessInstance();
  if (dataAccessptr) {
    dataAccessptr->initialize("access_config.json", "./");
  }
  //  tn::map::DataUtility::AccessInstance()->initialize("access_config.json",
  //  "",
  //                                                     cloudURL);
  //        traffic::TrafficService::InitializeService(
  //            QDir::current().path().toStdString(),
  //            QDir::current().path().toStdString(), &trafficUrl,
  //            "http://ec2s-autodenalitrafficservice-01.stg.mypna.com:8080",
  //            nullptr);
  //        traffic::TrafficService::SetLanguageAndUnit("en",
  //        traffic::TRAFFIC_EVENT_DESC_UNIT_MI);
  //        // m_traffic = traffic::TrafficService::GetInstance();

  //    static TnHttpProxy trafficUrl(
  //        Tn::Url::UrlCreator::GetInstance()->CreateUrl());

  //    m_traffic = traffic::TrafficService::GetInstance();
  //    m_traffic->InitializeService(
  //        QDir::current().path().toStdString(), &trafficUrl,
  //        "http://ec2s-autodenalitrafficservice-01.stg.mypna.com:8080",
  //        NULL);
  //    m_traffic->SetLanguageAndUnit("en",
  //    traffic::TRAFFIC_EVENT_DESC_UNIT_MI);

  const bool syncInit = false;
  tn::map::landmark::LandmarkDataUtility::AccessInstance()->setSyncInit(
      syncInit);
  tn::map::landmark::LandmarkDataUtility::AccessInstance()->initialise(
      "landmark", "", "", "landmark-data-access");
  tn::atlas::DataAccessInitParam param;
  param.dataAccess = tn::map::DataUtility::AccessInstance();
  param.landmarkAccess =
      tn::map::landmark::LandmarkDataUtility::AccessInstance();
  param.trafficService = traffic::TrafficService::GetInstance();
  m_data.reset(new tn::atlas::DataAccess(param));
}

void AtlasEngineRenderer::initAtlas() {

  tn::gfx::InitPointer init;
  //  init.getProcAddressPtr = &getProcAddr;
  auto validOpenGL = m_renderInterface.initialize(init);
  if (!validOpenGL) {
    qDebug() << "OpenGL: invalid!";
  }

  AtlasInitParams::path = std::string(std::getenv("ATLAS_RESOURCES_PATH"));
  QDir::setCurrent(QString::fromStdString(AtlasInitParams::path));
  qDebug() << "PATH: " << QString::fromStdString(AtlasInitParams::path);

  initData();
  if (!m_data) {
    return;
  }

  m_engine = tn::atlas::Engine::CreateMapEngine(
      m_renderInterface, &AtlasInitParams::addCWD, m_data);
  if (!m_engine) {
    return;
  }
  tn::atlas::ViewSettings viewSettings;
  initViewSettings(viewSettings);
  m_engine->Initialize(static_cast<uint32_t>(800), static_cast<uint32_t>(600));

  viewId = m_engine->CreateView(viewSettings);
  m_engine->Zoom(viewId, 12.0f);
  m_engine->LookAt(viewId, tn::proj::LatLonAlt(37.3862944, -122.005027));
}

void AtlasEngineRenderer::render() {
  if (!m_engine) {
    initAtlas();
  }
  m_engine->Render(tn::atlas::timestamp(
      static_cast<uint64_t>(QTime::currentTime().msecsSinceStartOfDay())));
  update();
}

void AtlasEngineRenderer::synchronize(QQuickFramebufferObject *item) {
  auto atlasEngine = static_cast<AtlasEngine *>(item);
  if (!atlasEngine || !m_engine) {
    initAtlas();
    return;
  }
  m_engine->ResizeView(viewId, 0, 0, atlasEngine->width() * 2,
                       atlasEngine->height() * 2);

  // sync translate
  m_engine->Translate(
      viewId, tn::atlas::Point2i(atlasEngine->m_dx * 2, atlasEngine->m_dy * 2));

  // sync angle
  m_engine->ZoomStep(viewId, atlasEngine->m_dAngle / 360.0f);
  // previous angle remains so it's forced back on 0 again
  atlasEngine->m_dAngle = 0;

  m_engine->GetViewState(viewId, atlasEngine->m_viewState);
  emit atlasEngine->stateChanged();
}

AtlasEngineRenderer::~AtlasEngineRenderer() {
  tn::map::DataUtility::AccessInstance()->shutdown();
  m_dataAccess.reset();
}

void AtlasEngine::propagateMouseMove(int x, int y, bool begin, bool end) {
  if (begin && !end) {
    m_x = x;
    m_y = y;
    return;
  }
  if (!begin && !end) {
    m_dx = x - m_x;
    m_dy = y - m_y;
    m_x = x;
    m_y = y;
    update();
  }
  if (!begin && end) {
    // not needed now
  }
}

void AtlasEngine::propagateMouseWheel(double angleDelta) {
  m_dAngle = angleDelta;
  update();
}

float AtlasEngine::zoom() { return m_viewState.zoomLevel.level; }
QGeoCoordinate AtlasEngine::center() {
  return QGeoCoordinate(m_viewState.cameraLatitude,
                        m_viewState.cameraLongitude);
}
QQuickFramebufferObject::Renderer *AtlasEngine::createRenderer() const {
  return new AtlasEngineRenderer;
}
