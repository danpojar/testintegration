cmake_minimum_required (VERSION 2.8)
project(OpenGL_QML_test)

set (CMAKE_CXX_STANDARD 11)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=address")
include_directories(${CMAKE_CURRENT_SOURCE_DIR})

#!!!!!!! Modify here
set (CMAKE_PREFIX_PATH "/Users/danp/Qt/5.8/clang_64")

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)
# Widgets finds its own dependencies (QtGui and QtCore).
find_package(Qt5Core)
get_target_property(QtCore_location Qt5::Core LOCATION)
find_package(Qt5Widgets)
get_target_property(QtCore_location Qt5::Widgets LOCATION)
find_package(Qt5Quick)
get_target_property(QtCore_location Qt5::Quick LOCATION)
find_package(Qt5Xml)
get_target_property(QtCore_location Qt5::Xml LOCATION)
find_package(Qt5Positioning)
get_target_property(QtCore_location Qt5::Positioning LOCATION)

#!!!!!!! Modify here
set(INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/modules/osx/include)
set(LIB_DIR ${CMAKE_CURRENT_SOURCE_DIR}/modules/osx/lib/debug)

#the sources:
set(SOURCES
	atlasengine.h
	atlasmapenginecommon.h
	mapdisplaycommontypes.h
    main.cpp
    atlasengine.cpp
)

set(ATLAS_LINK_FLAGS
    -L${LIB_DIR}
    tngfxengine
    tngfxrenderinterface
    tngfxmath
    glu
    tngfxutility
    )

set(TN_LINK_FLAGS
    -L${LIB_DIR}
    foundation
    boost_system
    tnmapdataaccess
    tnterrainaccess
    tnurl
    lz4
    minizip
    z
    boost_thread
    protobuf
    jpeg
    png
    sqlite3
    cjson
    curl
    freetype
    landmarkdataaccess
    traffic
    tinyxml
    boost_regex
    boost_filesystem
    bsdiff
    datastreamingmanager
    harfbuzz
    tnopenlr
    imgui
    )
include_directories (${Qt5Core_INCLUDE_DIRS})
include_directories (${Qt5Widgets_INCLUDE_DIRS})
include_directories (${Qt5Positioning_INCLUDE_DIRS})
include_directories (${INCLUDE_DIR})

qt5_add_resources(RESOURCES resources.qrc)
add_executable (${PROJECT_NAME} ${SOURCES} ${RESOURCES})

#dep libs
set(PLATFORM_LIBS_LINK_FLAGS
    "-framework Foundation"
    "-framework CoreFoundation"
    "-framework CoreGraphics"
    "-framework CoreText"
    "-framework OpenGL"
)

target_link_libraries(${PROJECT_NAME}
   Qt5::Core
   Qt5::Qml
   Qt5::Quick
   Qt5::Xml
   Qt5::Positioning
   Qt5::Widgets
)
target_link_libraries (${PROJECT_NAME} ${ATLAS_LINK_FLAGS})
target_link_libraries (${PROJECT_NAME} ${TN_LINK_FLAGS})
target_link_libraries (${PROJECT_NAME} ${PLATFORM_LIBS_LINK_FLAGS})
