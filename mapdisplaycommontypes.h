#ifndef MAPDISPLAYCOMMONTYPES_H
#define MAPDISPLAYCOMMONTYPES_H
namespace mapdisplay {
/**
 * @struct AtlasRect
 * @brief  AtlasRect struct needed for initialization copied from TnCommonData
 */

struct AtlasRect {
  AtlasRect() : x(0), y(0), dx(0), dy(0) {}
  AtlasRect(const AtlasRect &rt) {
    x = rt.x;
    y = rt.y;
    dx = rt.dx;
    dy = rt.dy;
  }
  AtlasRect &operator=(const AtlasRect &rt) {
    x = rt.x;
    y = rt.y;
    dx = rt.dx;
    dy = rt.dy;
    return *this;
  }

  bool operator==(const AtlasRect &rt) {
    return (x == rt.x && y == rt.y && dx == rt.dx && dy == rt.dy);
  }

  bool operator!=(const AtlasRect &rt) {
    return (x != rt.x || y != rt.y || dx != rt.dx || dy != rt.dy);
  }

  int x; /**<The horizontal coordinate for the beginning (bottom left corner) of
            the rectangle*/
  int y; /**<The vertical coordinate for the beginning (bottom left corner) of
            the rectangle*/
  int dx; /**<The width of the rectangle (in pixels).*/
  int dy; /**<The height of the rectangle (in pixels).*/
};
}
#endif // MAPDISPLAYMATH_H
