#ifndef ATLASENGINE_H
#define ATLASENGINE_H
#include "RenderEngine/DataAccess.h"
#include "RenderEngine/Engine.h"
#include "RenderEngine/EngineTypes.h"
#include "RenderInterface/RenderInterface.h"

#include <LandmarkDataAccess/TnMapLandmarkDataUtility.h>
#include <QGeoCoordinate>
#include <QOpenGLFramebufferObjectFormat>
#include <QQuickFramebufferObject>
#include <RenderEngine/DataAccess.h>

class AtlasEngineRenderer : public QQuickFramebufferObject::Renderer {
  tn::gfx::RenderInterface m_renderInterface;
  tn::atlas::Engine::ptr m_engine;
  tn::shared_ptr<tn::map::DataAccess> m_dataAccess;
  tn::atlas::DataAccessPtr m_data;
  tn::atlas::ViewId viewId;

  ~AtlasEngineRenderer();

  void initAtlas();
  void initData();
  void initViewSettings(tn::atlas::ViewSettings &viewSettings);
  QOpenGLFramebufferObject *createFramebufferObject(const QSize &size);
  void render();
  void synchronize(QQuickFramebufferObject *item);
};

class AtlasEngine : public QQuickFramebufferObject {
  Q_OBJECT
public:
  int m_x = 0, m_y = 0, m_dx = 0, m_dy = 0;
  double m_dAngle;
  tn::atlas::ViewState m_viewState;

  Q_PROPERTY(double zoom READ zoom NOTIFY stateChanged)
  Q_PROPERTY(QGeoCoordinate center READ center NOTIFY stateChanged)
  Q_INVOKABLE void propagateMouseMove(int x, int y, bool begin, bool end);
  Q_INVOKABLE void propagateMouseWheel(double angleDelta);

  float zoom();
  QGeoCoordinate center();

  QQuickFramebufferObject::Renderer *createRenderer() const;

signals:
  void stateChanged();
};

#endif
