#include "atlasengine.h"

#include <QDebug>
#include <QDir>
#include <QGuiApplication>
#include <QOpenGLContext>
#include <QQmlEngine>
#include <QQuickView>

void forceOpenGLVersion(int majorVersion, int minorVersion) {
  QSurfaceFormat format;
  format.setVersion(majorVersion, minorVersion);
  format.setMajorVersion(majorVersion);
  format.setMinorVersion(minorVersion);
  format.setProfile(QSurfaceFormat::CoreProfile);
  QSurfaceFormat::setDefaultFormat(format);
}

int main(int argc, char **argv) {
  const int majorVersion = 4;
  const int minorVersion = 1;
  forceOpenGLVersion(majorVersion, minorVersion);

  QGuiApplication app(argc, argv);
  QDir::setCurrent(QCoreApplication::applicationDirPath());

  qmlRegisterType<AtlasEngine>("atlasengine", 1, 0, "AtlasEngine");

  QQuickView qQuickView;
  qQuickView.setSource(QUrl::fromLocalFile(QDir::toNativeSeparators(
      QCoreApplication::applicationDirPath() + "/main.qml")));
  qQuickView.setWidth(800);
  qQuickView.setHeight(600);
  qQuickView.show();
  return app.exec();
}
