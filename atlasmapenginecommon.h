#ifndef ATLASMAPENGINECOMMON_H
#define ATLASMAPENGINECOMMON_H
#include "mapdisplaycommontypes.h"

#include <QDebug>
#include <QGeoCoordinate>
#include <string>

/**
 * @struct AtlasMapViewParams
 * @brief  AtlasMapViewParams contains initialization parameters
 */

struct AtlasInitParams {
  static std::string path;
  static const std::string addCWD(const std::string &file) {
    qDebug() << "CWD " << QString::fromStdString(
                              std::string(path).append("/").append(file));
    return std::string(path).append("/").append(file);
  }

  // dots per inch for normal view
  QGeoCoordinate initGeoCoordinate;
  mapdisplay::AtlasRect initRect;
  float initDpi = 162.0f;
  float screenScale = 1.0f;
  uint32_t baseTileSize = 256;
  std::string style = "newstyle.tss";
  std::string mapDataCloudUrl =
      "http://autodenali-ngxtile.telenav.com/map/v0/ngx/NA/15Q1/";
};

#endif // ATLASMAPENGINECOMMON
