#!/usr/bin/python
import os,sys,subprocess,atexit
from tools import *

# NavKit backend branch to match the ones used in NavKit releases
#sys.argv.append(r'--branch=rel-20170512-rc2')
sys.argv.append(r'--branch=feature/atlas-navkit-20170908')

currentWorkingDirPath = os.getcwd()

# This is a WORKAROUND
# The NavClient's library paths are hardcoded at linking time for the osx platform
# Therefore, the substitution to @rpath should be made
def setNavClientLibLinkPathsToRpath():
    logging.info( "Patching NavClient libraries hardcoded rpath..." )

    navClientDebugLibName = 'libnavclient---mt-sgd.dylib'
    navClientReleaseLibName = 'libnavclient---mt-s.dylib'

    modulesDir = os.path.join(currentWorkingDirPath, 'modules' )
    navkitLibDir = os.path.join( modulesDir, 'osx', 'lib')
    navClientDebugLibPath = os.path.join(navkitLibDir, 'debug', navClientDebugLibName)
    navClientReleaseLibPath = os.path.join(navkitLibDir, 'release', navClientReleaseLibName)

    if os.path.exists(navClientDebugLibPath):
        logging.info("Patching debug libraries")
        os.system("install_name_tool -id @rpath/%s %s" %(navClientDebugLibName, navClientDebugLibPath))

    if os.path.exists(navClientReleaseLibPath):
        logging.info("Patching release libraries")
        os.system("install_name_tool -id @rpath/%s %s" %(navClientReleaseLibName, navClientReleaseLibPath))

    logging.info( "Done patching NavClient libraries" )

class QtArp(Component):
    # For third-party components use their release revision as base revision,
    # but omit anything other then numbers and dots. E.g.  WebKit 1.0.2b12
    # becomes WebKit 1.0.212. Publish command will add our own SVN revision
    # numer at the end of base revision to form the full revision number.
    baseRevision = '0.1'

    # If project name in CMakeLists file does not match your component
    # name (which might happen if you're using third-party CMakeLists.txt),
    # you can specify it here. If you're writing your own CMakeLists make
    # names the same and omit this option.

    cmakeProjectName = ''

    # Dependencies
    dependencies = [
#        StaticLibraryDependency( componentName = 'NavKit', baseRevision = '2.58.496748' )
        StaticLibraryDependency( componentName = 'AtlasMapDisplayGraphics')
    ]

if __name__ == '__main__':
    if sys.platform == 'darwin':
        atexit.register(setNavClientLibLinkPathsToRpath)

    # sourceDir tells build tools to read source files from the given directory.
    # By default it looks for source files in the same directory as your
    # build.py file.
    main( sourceDir = os.path.dirname(os.path.realpath(sys.argv[0])))
